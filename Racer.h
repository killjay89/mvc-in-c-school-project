/**
* File:   Racer.h
* @author: Michal Hat�k
*
* Created on 29. prosinec 2011, 4:07
*/

#ifndef RACER_H
#define	RACER_H

#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Racer 
{
	

	private:
		string name;
		string delimiter;
		vector<float> score;

	public:
		/**
		* constructor
		*/
		Racer(string n) 
		{
			this->name = n;
		}

		/**
		* set races name
		*/
		void setName(string n) 
		{
			this->name = n;
		}

		/**
		* set delimiter
		*/
		void setDelimiter(string d)
		{
			delimiter = d;
		}

		/**
		* get count of scores
		* @return int
		*/
		int getCount(void) const
		{
			return this->score.size();
		}

		/**
		* get name of racer
		* @return string
		*/
		string getName(void) const
		{
			return this->name;
		}

		/**
		* set delimiter
		*/
		string getDelimiter(void) const
		{
			return delimiter;
		}
		
		/**
		* insert new score
		*/
		void newScore(float value)
		{
			this->score.push_back(value);
		}

		/**
		* get total score
		* @return float
		*/
		float getScore() const
		{
			if(this->score.empty())
				return 0;

			float temp = 0;
			for(int i = 0; i < static_cast<signed int>(this->score.size()); i++)
			{
				temp += score[i];
			}
			return temp;
		}

		/**
		* get total average score
		* @return float
		*/
		float getAvgScore() const
		{
			if(this->getScoreCount() == 0)
				return 0;
			return this->getScore()/this->getScoreCount();
		}

		/**
		* get score count
		* @return int
		*/
		int getScoreCount() const
		{
			return this->score.size();
		}

		/**
		* overloaded operators
		* @return bool
		*/
		friend bool operator==( const Racer&, const Racer&);
		friend bool operator<(const Racer&, const Racer&);
		friend bool operator>(const Racer&, const Racer&);
		friend bool operator<=(const Racer&, const Racer&);
		friend bool operator>=(const Racer&, const Racer&);

		friend ostream& operator<<(ostream&, const Racer&);
	
};

#endif	/* RACER_H */
