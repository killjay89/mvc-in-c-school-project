/**
 * File:   TScoreboard.h
 * @author: Michal Hat�k
 *
 * Created on 29. prosinec 2011, 4:07
 */


#ifndef DISPLAY_H
#define	DISPLAY_H

#include "Display.h"

#endif

#include "Racer.h"

#ifndef TSCOREBOARD_H
#define	TSCOREBOARD_H

#include <vector>

using namespace std;

class TScoreboard
{   // model
    private:

    protected:
        vector<Racer> m_items;

    public:
        TScoreboard();

        int Add(string);

        Racer& getItem(int);

        void Clear(void);

        int Count(void);

        void Swap(int, int);

        bool Cmp(int, int);

        bool Sort(void);
        void quicksort(int, int );

        bool newScore(int, float);

        Racer& operator [](int);
        
	
		string List(string );

 };

#endif	/* TSCOREBOARD_H */

