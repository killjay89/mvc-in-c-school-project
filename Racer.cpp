#include "Racer.h"




bool operator==(const Racer& r1, const Racer& r2)
{
	if(r1.getScore() == r2.getScore())
		return true;
	else
		return false;
}

bool operator<(const Racer& r1, const Racer& r2)
{
	if(r1.getScore() < r2.getScore())
		return true;
	else
		return false;
}

bool operator>(const Racer& r1, const Racer& r2)
{
	if(r1.getScore() > r2.getScore())
		return true;
	else
		return false;
}

bool operator>=(const Racer& r1, const Racer& r2)
{
	if(r1.getScore() >= r2.getScore())
		return true;
	else
		return false;
}

bool operator<=(const Racer& r1, const Racer& r2)
{
	if(r1.getScore() <= r2.getScore())
		return true;
	else
		return false;
}

ostream& operator<<(ostream& os, const Racer& r)
{
	os << r.getName() << r.getDelimiter() << r.getAvgScore() << r.getDelimiter() << r.getCount() << endl;

	return os;
}